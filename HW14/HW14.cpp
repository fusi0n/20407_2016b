/**
* Or Groman 2016
* 20407 2016B
* HW 14
*/

#include <iostream>
#include <iterator>
#include <algorithm>	// std::swap
#include <stdlib.h>     // srand, rand
#include <time.h>       // time
#include <cmath>

#define MAXIMAL_N_VALUE  401

int max_stack=0;


/*function declarations*/
int partition(int A[],int left,int right,int pivotIndex);
int randomSelect(int A[],int left,int right,int k);
void quickSort(int A[],int left,int right,int k,int depth);
int randomPartition(int A[],int left,int right);


int randomSelect(int A[],int left,int right,int k)
{
	int pivotIndex;
	if(left==right)		// If the list contains only one element,
		return left;	// return that element


	pivotIndex = randomPartition(A,left,right);		// The pivot is in its final sorted position

	if(k==pivotIndex)
		return pivotIndex;				//pivot IS the kth element, return pivot index

	else if (k<pivotIndex)
		return randomSelect(A,left,pivotIndex-1,k);	//select over A[left...pivotIndex-1]
	else
		return randomSelect(A,pivotIndex+1,right,k);	//select over A[pivotIndex+1...right]

}
int randomPartition(int A[],int left,int right)
{
	int pivotIndex;

	srand (time(NULL)); 	// initialize random seed
	pivotIndex = (rand() % ((right-left)+1)) + left;	// select a pivotIndex between left and right

	pivotIndex = partition(A,left,right,pivotIndex);
	return pivotIndex;

}
int partition(int A[],int left,int right,int pivotIndex)
{
	int pivot=A[pivotIndex];
	int temp;
	int i;

	std::swap(A[pivotIndex],A[right]); //Move pivot to end
	temp = left;
	for(i=left;i<right;i++)
	{
		if(A[i]<pivot)
		{
			std::swap(A[temp],A[i]);
			temp++;
		}
	}
	std::swap(A[right],A[temp]); 	//Move pivot to its final place
	return temp;			//return new index of pivot
}
void quickSort(int A[],int left,int right,int k,int depth)
{
	int pivot;
	int t;
	depth++;
	if(max_stack<depth)
		max_stack=depth;
	if(k>(right-left) || k==0 )
		t=0;		
	else
		t=std::ceil((float)(right-left)/k);	

	if(left<right)
	{
		pivot=randomSelect(A,left,right,left+t);	
		quickSort(A,left,pivot-1,k,depth);
		quickSort(A,pivot+1,right,k,depth);
	}

} 

int main()
{
	int A[MAXIMAL_N_VALUE];
	int temp[MAXIMAL_N_VALUE];
	int k=0;
	int n;	
	int i;
	
	/*build random array */
	srand (time(NULL));	 	// initialize random seed
	for(i=0;i<MAXIMAL_N_VALUE;i++)
		A[i]=rand() %  10000;	// Fill in A with random values




	for(i=0;i<MAXIMAL_N_VALUE;i++)
		temp[i]=A[i];		// quicksort will sort "in place" so a temp array needed 
			

	/*
		This entire section of the code is just asking from the
		user to choose values for n and for k
		once an option was picked (1,2,3,4...)
		k and n will change there values according to the known constants.	
		
	*/

	std::cout << "pick k value: \n 1. k=1 \n 2. k=m/8 \n 3. k=m/4 \n 4. k=m/2 \n";
	std::cin >> k;	
	switch (k)
	{
		case 1:	k=0; break;
		case 2: k=8; break;
		case 3:	k=4; break;
		case 4: k=2; break;
		default:
			std::cout << "error while picking k value";		
	}
	std::cout << "pick n value: \n 1. n=100 \n 2. n=200 \n 3. n=400 \n";
	std::cin >> n;
	switch (n)
	{
		case 1:	n=100; break;
		case 2:	n=200; break;
		case 3: n=400; break;
		default:
			std::cout << "error while picking n value";
	}

	quickSort(A,1,n,k,0);		
	std::cout << "max recursive calls: ";
	std::cout << max_stack;
	std::cout << std::endl;

	return 0;
}
